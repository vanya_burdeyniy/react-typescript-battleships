import * as React from 'react';
import { Header, BattleShips } from './components';

const ships = [[4, 4, 1, 1], [4, 4, 1, 1]];

export const App: React.StatelessComponent<{}> = () => {
  return (
    <div>
      <Header />
      <BattleShips ships={ships} />
    </div>
  );
}
