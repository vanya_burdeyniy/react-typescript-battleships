const update = require('react-addons-update');

export default (function () {
    const types = Object.freeze({
        HIT: 3,
        SHIP: 2,
        MISS: 1,
        EMPTY: 0
    });

    let playersShips = {
        0: [],
        1: []
    };

    function createShips(ships, playerIndex) {
        let field = Array.from(new Array(10), ()=> Array.from(new Array(10), () => 0));
        let availableCoords = {
            x: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            y: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        };

        let playerCoords = setCoords(ships, availableCoords);

        playersShips[playerIndex] = playerCoords;

        playerCoords.forEach(([row, col]) => {
            field[row][col] = types.SHIP;
        });

        return field;
    }

    function attackToCell(field, row, col) {
        let newField = field;

        switch(field[row][col]) {

            case types.EMPTY:
                newField = update(field, { [row]: { [col]: { $set: types.MISS}}});
                break;

            case types.SHIP:
                newField = update(field, { [row]: { [col]: { $set: types.HIT}}});
                break;

            case types.HIT:
            case types.MISS:
                break;

        }

        return newField;
    }

    function setCoords (ships, availableCoords) {
        let coords = [];

        ships.forEach((shipLength, index) => {
            let row = availableCoords.x[Math.floor(Math.random() * availableCoords.x.length)];
            let col = availableCoords.y[Math.floor(Math.random() * availableCoords.y.length)];

            if (shipLength === 4 && col > 6) col = 9 - shipLength;

            for (let i = 0; i < shipLength; i++) {
                if (index === 0 && i === shipLength - 1) {
                    coords.push([row === 9 ? row - 1 : row + 1, col + i - 1]);
                } else {
                    coords.push([row, col + i]);
                }
            }

            removeCurrentRowCoord(row, availableCoords);
            if (index === 0) {
                removeCurrentRowCoord(row === 9 ? row - 1 : row + 1, availableCoords)
            }
        });

        return coords;
    }

    function removeCurrentRowCoord(row, availableCoords) {
        let rowIndex = availableCoords.x.indexOf(row);

        if (rowIndex === -1) return;

        switch(rowIndex) {

            case 0:
                availableCoords.x = availableCoords.x.filter(coord => coord !== availableCoords.x[rowIndex] &&
                    coord !== availableCoords.x[rowIndex+1]);
                break;

            case availableCoords.x.length - 1:
                availableCoords.x = availableCoords.x.filter(coord => coord !== availableCoords.x[rowIndex] &&
                    coord !== availableCoords.x[rowIndex-1]);
                break;

            default:
                availableCoords.x = availableCoords.x.filter(coord => coord !== availableCoords.x[rowIndex] &&
                    coord !== availableCoords.x[rowIndex+1] && coord !== availableCoords.x[rowIndex-1])

        }
    }

    function isWinner (field, playerIndex) {
        let isWin = true;

        playersShips[playerIndex].forEach(([row, col]) => {
            if (field[row][col] !== this.types.HIT) {
                isWin = false;
            }
        });

        return isWin;
    };

    return {
        types: types,
        createShips: createShips,
        attackToCell: attackToCell,
        isWinner: isWinner
    };
})();

