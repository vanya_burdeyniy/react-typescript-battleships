import * as React from 'react';
import BattleshipFieldHelper from '../helpers/BattleShipFieldHelper.js';
import {BattleShipField} from "./BattleShipField";
const update = require('react-addons-update');

export class BattleShips extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.fire = this.fire.bind(this);
        this.switchTurn = this.switchTurn.bind(this);
        this.switchPlayer = this.switchPlayer.bind(this);
        this.callFire = this.callFire.bind(this);
        this.createCoordsToFire = this.createCoordsToFire.bind(this);
        this.cloneCoords = this.cloneCoords.bind(this);

        this.state = {
            whoseTurn: 0,
            hasFired: false,
            gameOver: false,
            winner: null,
            playerBoards: [BattleshipFieldHelper.createShips(this.props.ships[1], 1), BattleshipFieldHelper.createShips(this.props.ships[0], 0)],
            coordsToFirePlayer0: this.createCoordsToFire(),
            coordsToFirePlayer1: this.createCoordsToFire()
        };
    }

    public switchPlayer () {
        return (this.state.whoseTurn === 0) ? 1 : 0;
    }

    public createCoordsToFire() {
        let defaultCoords = [];
        let coords = [];

        for (let i = 0; i < 10; i++) {
            defaultCoords.push({coords: [0, i], id: 'id_'+0+i});
        }
        for (let i = 0; i < 10; i++) {
            coords = coords.concat(this.cloneCoords(defaultCoords, i));
        }

        return coords;
    }

    public cloneCoords(arr: any, index: number) {
        let newArr = JSON.parse(JSON.stringify(arr));

        for (let i = 0; i < newArr.length; i++) {
            newArr[i].coords[0] = index;
            newArr[i].id = 'id_' + index + newArr[i].coords[1];
        }
        return newArr;
    }

    public fire (row: any, col: any) {
        if (this.state.hasFired) { return; }

        let boards = this.state.playerBoards,
            player = this.state.whoseTurn,
            board = this.state.playerBoards[player],
            newBoard = BattleshipFieldHelper.attackToCell(board, row, col),
            isGameOver = BattleshipFieldHelper.isWinner(
                newBoard,
                this.switchPlayer());

        let newBoards = update(boards, { [player]: { $set: newBoard } });

        let newState = {
            hasFired: true,
            playerBoards: newBoards,
            gameOver: isGameOver
        };

        if (isGameOver) {
            newState['winner'] = player;
            this.setState(newState);
        } else {
            this.callFire(newBoards, isGameOver);
        }
    }

    public callFire(newBoards: any, isGameOver: any) {
        let coordsToFireTurn = this.switchPlayer() === 1 ? 'coordsToFirePlayer1' : 'coordsToFirePlayer0'
        let randomCoordsToFire = JSON.parse(JSON.stringify(this.state[coordsToFireTurn][Math.floor(Math.random() * this.state[coordsToFireTurn].length)]));

        this.setState({
            whoseTurn: this.switchPlayer(),
            playerBoards: newBoards,
            gameOver: isGameOver,
            coordsToFirePlayer0: this.switchPlayer() === 0 ? this.state.coordsToFirePlayer0.filter(currentCoord => currentCoord.id !== randomCoordsToFire.id) :
                this.state.coordsToFirePlayer0,
            coordsToFirePlayer1: this.switchPlayer() === 1 ? this.state.coordsToFirePlayer1.filter(currentCoord => currentCoord.id !== randomCoordsToFire.id) :
                this.state.coordsToFirePlayer1
        }, () => {
            setTimeout(() => {
                this.fire(randomCoordsToFire.coords[0], randomCoordsToFire.coords[1]);
            }, 150);
        });
    }

    public switchTurn () {
        this.setState({
            whoseTurn: this.switchPlayer(),
            hasFired: false
        });
    }

    public getRandomInt(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public start() {
        this.fire(this.getRandomInt(0, 9), this.getRandomInt(0, 9));
    }

    public render() {
        const {gameOver, whoseTurn} = this.state;

        return (
            <div className="battleship">
                <h1>{whoseTurn === 0 ? 'Player 1' : 'Player 2'}</h1>
                <BattleShipField field={this.state.playerBoards[0]}
                                 canFire={!this.state.hasFired}
                                 onFire={this.fire.bind(this)}
                                 visible={whoseTurn === 0}/>
                <BattleShipField field={this.state.playerBoards[1]}
                                 canFire={!this.state.hasFired}
                                 onFire={this.fire.bind(this)}
                                 visible={whoseTurn === 1}/>
                {gameOver ? <h1>Player {this.state.winner + 1} wins!</h1> : ''}
                <button className="switch-btn" onClick={this.start.bind(this)}>Start</button>
            </div>
        )
    }
}
