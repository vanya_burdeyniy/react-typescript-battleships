import * as React from 'react';
import BattleshipFieldHelper from '../helpers/BattleShipFieldHelper.js';

export class BattleShipField extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
    }

    public renderSquare(row: any, col: any) {
        switch(this.props.field[row][col]) {

            case BattleshipFieldHelper.types.EMPTY:
            case BattleshipFieldHelper.types.SHIP:
                return (
                    <div className="empty" key={col}
                         onClick={this.props.onFire.bind(this, row, col)} />
                );

            case BattleshipFieldHelper.types.HIT:
                return <div className="hit" key={col} />;

            case BattleshipFieldHelper.types.MISS:
                return <div className="miss" key={col} />;

        }
    }

    public render() {
        let rows = [];

        for (let row = 0; row < 10; row++) {
            let currentRow = [];

            for (let col=0; col<10; col++) {
                let square = this.renderSquare(row, col);
                currentRow.push(square);
            }

            rows.push(<div className="game-row" key={row}>{currentRow}</div>);
        }

        return (
            <div className={this.props.visible ? '' : 'hidden'}>
                {rows}
            </div>
        );
    }
}
