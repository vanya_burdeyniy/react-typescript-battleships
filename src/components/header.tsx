import * as React from 'react';

export const Header: React.StatelessComponent<{}> = () => {
  return (
      <header>
          <h2>Battleship</h2>
      </header>
  );
}
